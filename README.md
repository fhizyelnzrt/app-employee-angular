# Sampel Viva.co.id Company

Aplikasi ini hanya untuk Sampel untuk pengelolaan karyawan dengan data dummy atau fake data menggunakan framework Angular jangan lupa sebelum menjalankan melakukan `npm install` dan `npm install json-server` terlebih dahulu baru jalankan. jika ada yg ditanyakan silakah kontak admin atau kujungi `https://fhizyelnazareta.github.io` dan untuk melihat contoh project saya yang lain bisa di `https://www.thevivanetworks.com/prototype/`

## App Employee Management

Aplikasi yang dibuat dengan [Angular CLI]

## Development server dan Running App

Jalanlan `ng serve` atau `npm start` untuk running angular diport `http://localhost:4200/`. dan jalankan `json-server --watch db-employee.json` untuk menjalankan data backend/database yang disimpan di file json

## Auth login ke aplikasi
username: admin
password: admin123

## Sumber Data Dummy

Sumber data diambil dari ChatGPT dengan generate request seperti 
"Buatkan saya json karyawan yang berisi id, username, password, firstName, lastName, email, dob dengan format tanggal lahir, gender dengan isi Perempuan atau Laki-laki, salary, status, group & description isinya status karyawan active atau nonactive dan isi group yaitu 'Product', 'Design', 'Developer', 'Infrastruktur', 'IT Supporting', 'Traffic Web', 'Seo Division', 'Redaksi', 'F&B', 'Sales', 'Marketing', 'Sosial Media', 'Multimedia', 'Content Writing',  dengan jumlah 10 data indonesia.".

## Copyright 2024 Fhizyel Nazareta
