import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _fb: FormBuilder,
    private router: Router
  ){
    this.personLoginForm = this._fb.group({
      username: '',
      password: '',
    });
  }

  ngOnInit(): void {
  }

  gotoHomePage(){
    this.router.navigateByUrl('');
  }

  // Get data dari form login
  personLoginForm = new FormGroup({
    username: new FormControl("username"),
    password: new FormControl("password"),
    })

  async onLogin(){

    // Cek auth login admin simpel
    if(this.personLoginForm.value.username === "admin"){
      if(this.personLoginForm.value.password === "admin123"){
        await sessionStorage.setItem('credentials', 'username: admin',);
        this.gotoHomePage()
      }else{
        alert('username/password salah');
      }
    }else{
      alert('username/password salah');
    }

  }

}
