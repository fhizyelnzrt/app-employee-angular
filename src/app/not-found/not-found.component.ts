import { Component } from '@angular/core';
import { ActivatedRoute, Route, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  constructor(
    private route: ActivatedRoute,
    private _location: Location,
    private router: Router
  ){
  }

  gotoHomePage(){
    this.router.navigateByUrl('');
  }

}
