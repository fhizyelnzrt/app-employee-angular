import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Route, Router} from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { EmpAddEditComponent } from '../emp-add-edit/emp-add-edit.component';
import {Location} from '@angular/common';


@Component({
  selector: 'app-emp-detail',
  templateUrl: './emp-detail.component.html',
  styleUrls: ['./emp-detail.component.scss']
})
export class EmpDetailComponent implements OnInit {

  empId!: string;
  dataSource!: any;
  dataEmp!: any;


  constructor(
    private route: ActivatedRoute,
    private _dialog: MatDialog,
    private _empService: EmployeeService,
    private _location: Location
  ){
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params)=> {
      this.empId = params.get('empId')!;
      this.getDetailEmployeeList(this.empId);
    });
  }

  getDetailEmployeeList(empId : any) {
    this._empService.getEmployeeList().subscribe({
      next: (res) => {
        this.dataSource = res.filter((data : any) => data.id === empId);
        this.dataEmp = this.dataSource[0];
      },
      error: console.log,
    });
  }

  
  openEditForm(data: any) {
    const dialogRef = this._dialog.open(EmpAddEditComponent, {
      data,
    });

    dialogRef.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.route.paramMap.subscribe((params)=> {
            this.empId = params.get('empId')!;
            this.getDetailEmployeeList(this.empId);
          });
        }
      },
    });
  } 

  goBack() {
    this._location.back();
  }

}
