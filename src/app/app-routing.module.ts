import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EmpDetailComponent } from './emp-detail/emp-detail.component';
import { EmpListComponent } from './emp-list/emp-list.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: '', component: EmpListComponent, data:{pageTitle:"Home Page", isLogin:'yes'}, canActivate: [AuthGuardService]
  },
  {
    path: 'search/:search', component: EmpListComponent, data:{pageTitle:"Home Page", isLogin:'yes'}, canActivate: [AuthGuardService]
  },
  {
    path: 'employee/:empId', component: EmpDetailComponent, data:{pageTitle:"Detail Page", isLogin:'yes'}, canActivate: [AuthGuardService]
  },
  {
    path: 'login', component: LoginComponent, data:{pageTitle:"Masuk", isLogin:'no'}
  },
  {
    path: '**', component: NotFoundComponent, data:{pageTitle:"Page Not Found", isLogin:'no'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
