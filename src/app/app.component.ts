import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EmpAddEditComponent } from './emp-add-edit/emp-add-edit.component';
import { EmployeeService } from './services/employee.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CoreService } from './core/core.service';
import { Route, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  displayedColumns: string[] = [
    'userName',
    'firstName',
    'lastName',
    'email',
    'dob',
    'gender',
    'group',
    'description',
    'salary',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private _dialog: MatDialog,
    private _empService: EmployeeService,
    private _coreService: CoreService,
    private router: Router,
    private route : ActivatedRoute
  ) {}
  

  isLogin: any

  ngOnInit(): void {
    this.logoutBtn();
    setInterval(() => {
      this.logoutBtn(); 
      }, 1000);
  }

  logoutBtn(){
    if(sessionStorage != null){
      sessionStorage.getItem('credentials') ? this.isLogin = true : this.isLogin = false ;
    }
  }

  destroySessionLogin(){
    sessionStorage.clear();
    this.router.navigateByUrl(`login`);
  }

}
